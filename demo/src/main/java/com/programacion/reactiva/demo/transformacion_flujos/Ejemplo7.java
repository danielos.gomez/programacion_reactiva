package com.programacion.reactiva.demo.transformacion_flujos;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.ArrayList;
import java.util.List;

public class Ejemplo7 {
    public static void main(String[] args) {

        ArrayList<String> myArrayList = new ArrayList<>(List.of("Isabella", "Juan" , "Daniel","Mabel"));

        Flux.fromIterable(myArrayList)
                .filter(nombre-> nombre.length() >4)
                .flatMap(Ejemplo7::convertirNombreModificadoEnMono)
                .subscribe(System.out::println);

    }

    private static Mono<String>convertirNombreModificadoEnMono(String nombre){
        return Mono.just(nombre.concat(" - Modificado"));
    }
}
