package com.programacion.reactiva.demo;

import org.junit.jupiter.api.Test;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.test.StepVerifier;

import java.time.Duration;
import java.util.ArrayList;
import java.util.List;


public class EjemploTestFlujos {
    @Test
    public void testTransforMap(){
        ArrayList<String> myArrayList = new ArrayList<>(List.of("Isabella", "Juan" , "Daniel","Mabel"));
        Flux<String> nombresFlux= Flux.fromIterable(myArrayList)
                .filter(nombre-> nombre.length()>4)
                .map(String::toUpperCase)
                .log();

        StepVerifier.create(nombresFlux)
                .expectNext("ISABELLA" , "DANIEL","MABEL")
                .verifyComplete();



    }

    @Test
    public void testTransforFlatMap(){
        ArrayList<String> myArrayList = new ArrayList<>(List.of("Isabella", "Juan" , "Daniel","Mabel"));
        Flux<String> nombresFlux= Flux.fromIterable(myArrayList)
                .filter(nombre-> nombre.length()>4)
                .flatMap(nombre -> { //convierte en un flujo mono
                    return Mono.just(nombre.toUpperCase());
                })
                .log();

        StepVerifier.create(nombresFlux)
                .expectNext("ISABELLA" , "DANIEL","MABEL")
                .verifyComplete();

    }

    @Test
    public void testCombinarFlujos(){
        ArrayList<String> myArrayList1 = new ArrayList<>(List.of("Juan", "Cesar" , "Daniel"));
        ArrayList<String> myArrayList2 = new ArrayList<>(List.of("Carlos", "Emely" , "Diana"));

        Flux<String> flux1 = Flux.fromIterable(myArrayList1);
        Flux<String> flux2 = Flux.fromIterable(myArrayList2);


        Flux<String> mergeFlux = Flux.merge(flux1, flux2)
                .map(nombre->
                     nombre.toUpperCase()
                ).log();

        StepVerifier.create(mergeFlux)
                .expectNext("JUAN" , "CESAR","DANIEL","CARLOS")
                .thenCancel()
                .verify();

    }

    @Test
    public void testCombinarFlujosConDelay(){
        ArrayList<String> myArrayList1 = new ArrayList<>(List.of("Juan", "Cesar" , "Daniel"));
        ArrayList<String> myArrayList2 = new ArrayList<>(List.of("Carlos", "Emely" , "Diana"));

        Flux<String> flux1 = Flux.fromIterable(myArrayList1).delayElements(Duration.ofSeconds(6));
        Flux<String> flux2 = Flux.fromIterable(myArrayList2).delayElements(Duration.ofSeconds(3));;


        Flux<String> mergeFlux = Flux.merge(flux1, flux2)
                .map(nombre->
                        nombre.toUpperCase()
                ).log();

        StepVerifier.create(mergeFlux)
                .expectSubscription()
                .expectNextCount(6L)
                .verifyComplete();

    }


    @Test
    public void testUnirFlujosConDelay(){

        ArrayList<String> myArrayList1 = new ArrayList<>(List.of("Juan", "Cesar" , "Daniel"));
        ArrayList<String> myArrayList2 = new ArrayList<>(List.of("Carlos", "Emely" , "Diana"));
        String myString = "Yennifer";

        Flux<String> flux1 = Flux.fromIterable(myArrayList1).delayElements(Duration.ofSeconds(1));
        Flux<String> flux2 = Flux.fromIterable(myArrayList2).delayElements(Duration.ofSeconds(2));;
        Mono<String> mono1 = Mono.just(myString);


        Flux<String> mergeFlux = Flux.concat(flux1, flux2,mono1)
                .map(nombre->
                        nombre.toUpperCase()
                ).log();

        StepVerifier.create(mergeFlux)
                .expectSubscription()
                .expectNextCount(7L)
                .verifyComplete();

    }


    @Test
    public void testCombinarFlujosConZip(){

        ArrayList<String> myArrayList1 = new ArrayList<>(List.of("Juan", "Cesar" , "Daniel", "Yen"));
        ArrayList<String> myArrayList2 = new ArrayList<>(List.of("Carlos", "Emely" , "Diana"));

        Flux<String> flux1 = Flux.fromIterable(myArrayList1).delayElements(Duration.ofSeconds(1));
        Flux<String> flux2 = Flux.fromIterable(myArrayList2).delayElements(Duration.ofSeconds(2));;


        Flux<String> mergeFlux = Flux.zip(flux1, flux2 , (f1,f2)-> {
            return f1.concat(" ").concat(f2).toUpperCase();
        }).log();

        StepVerifier.create(mergeFlux)
                .expectSubscription()
                .expectNextCount(3L)
                .verifyComplete();

    }
}
