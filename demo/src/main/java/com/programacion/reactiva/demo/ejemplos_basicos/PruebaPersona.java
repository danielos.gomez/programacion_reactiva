package com.programacion.reactiva.demo.ejemplos_basicos;

import com.programacion.reactiva.demo.models.Persona;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import reactor.core.publisher.Flux;

public class PruebaPersona {

    private static final Logger log = LoggerFactory.getLogger(PruebaPersona.class);

    public static void main(String[] args) {

        Flux<String> nombres = Flux.just("Daniel Ochoa", "Cesar Pava" , "Juan Garzon" , "Carlos Munera");

        Flux<Persona> personaFlux = nombres.map(
                nombre-> new Persona(nombre.split(" ")[0].toUpperCase(),nombre.split(" ")[1].toUpperCase()))
                .filter(persona -> persona.getApellido().equalsIgnoreCase("gomez"))
                .doOnNext(persona -> {
                    if (persona==null){
                        throw new RuntimeException("No puede estar vacio");
                    }
                    System.out.println(persona.getApellido() +" "+persona.getNombre());
                } );

        personaFlux.subscribe(
                persona -> log.info(persona.toString().toLowerCase()) //onNext
                , e -> log.error(e.getMessage()) //OnError
                , new Runnable() { //onComplete
                    @Override
                    public void run() {
                        log.info("Se ha completado correctamente");
                    }
                }

        );

    }
}
