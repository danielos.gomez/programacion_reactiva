package com.programacion.reactiva.demo.webflux_anotaciones;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.reactive.function.server.*;

@Configuration
public class HelloRouter {

    @Bean
    public RouterFunction<ServerResponse> routerFunction (HelloHandler helloHandler){
        return RouterFunctions.route(RequestPredicates.GET("/functional/mono"),helloHandler::MsjMono)
                .andRoute(RequestPredicates.GET("/functional/flux"),helloHandler::MsjFlux);
    }
}
