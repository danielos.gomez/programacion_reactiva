package com.programacion.reactiva.demo;

import org.junit.jupiter.api.Test;
import reactor.core.publisher.Flux;
import reactor.test.StepVerifier;

public class EjemploTests {

    @Test
    public void testFlux(){
        Flux<Integer> fluxTest =Flux.just(1,2,3,4,5);

        //creaamos la prueba

        StepVerifier.create(fluxTest)
                .expectNext(1)
                .expectNext(2)
                .expectNext(3)
                .expectNextCount(2)
                .expectComplete()
                .verify();
    }

    @Test
    public void testFlux2(){
        Flux<Integer> fluxTest =Flux.just(1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20);

        //creaamos la prueba

        StepVerifier.create(fluxTest)
                .expectNext(1)
                .expectNext(2)
                .expectNext(3)
                .thenCancel()
                .verify();
    }
                                          

    @Test
    public void testFluxString(){
        Flux<String> myFlux = Flux.just("Daniel", "Juan", "Cesar")
                .filter(x-> x.length()<=5)
                .map(String::toUpperCase);

        StepVerifier.create(myFlux)
                .expectNext("JUAN")
                .expectNextMatches(s->s.length()>0)
                .expectComplete()
                .verify();
    }


}
