package com.programacion.reactiva.demo.webflux_anotaciones;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.time.Duration;

@RestController
@RequestMapping("/hello")
public class HelloController {

    @GetMapping("/mono")
    public Mono<String> getMono(){
        return Mono.just("Hola Cesar y Juan");
    }

    @GetMapping("/flux")
    public Flux<String> getFlux(){
        Flux<String> msj = Flux.just("Hola ", "Cesar ", "Juan ","").delayElements(Duration.ofSeconds(3));
        return msj;
    }
}
