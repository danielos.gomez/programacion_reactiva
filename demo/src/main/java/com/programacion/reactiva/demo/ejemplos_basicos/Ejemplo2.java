package com.programacion.reactiva.demo.ejemplos_basicos;

import reactor.core.publisher.Mono;

public class Ejemplo2 {
    public static void main(String[] args) {
        //Mono
        Mono<String> mono = Mono.just("Daniel") ;
        mono.subscribe(
                s -> System.out.println(s.toUpperCase()), //onNext
                throwable -> System.out.println("Error : "+throwable),//onError
                () -> System.out.println("Completed")//onComplete
        );

    }
}
