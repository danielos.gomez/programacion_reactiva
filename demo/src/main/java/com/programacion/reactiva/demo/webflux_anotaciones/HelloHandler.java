package com.programacion.reactiva.demo.webflux_anotaciones;

import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.server.ServerRequest;
import org.springframework.web.reactive.function.server.ServerResponse;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.time.Duration;

@Component
public class HelloHandler {

    public Mono<ServerResponse> MsjMono(ServerRequest serverRequest){
        return ServerResponse.ok()
                .contentType(MediaType.TEXT_PLAIN)
                .body(
                Mono.just("Hola Cesar y Juan"),String.class
                );
    }

    public Mono<ServerResponse> MsjFlux(ServerRequest serverRequest){
        return ServerResponse.ok()
                .contentType(MediaType.APPLICATION_STREAM_JSON)
                .body(
                        Flux.just("Hola","Cesar","Juan").delayElements(Duration.ofSeconds(2)),String.class
                );
    }

}
