package com.programacion.reactiva.demo.ejemplo_thymeleaf;

import org.springframework.stereotype.Repository;
import reactor.core.publisher.Flux;

import java.time.Duration;
import java.util.ArrayList;
import java.util.List;

@Repository
public class ProductoRepository {
    private  static List<Producto> list = new ArrayList<>();

    private  static List<Producto> list2 = new ArrayList<>();

    static {
        list.add(new Producto(1L,"Portatil",2500));
        list.add(new Producto(2L,"Camara",500));
        list.add(new Producto(3L,"Monitor",800));
        list.add(new Producto(4L,"Audifonos",200));
        list.add(new Producto(5L,"Teclado",100));

        list2.add(new Producto(6L,"PS4",800));
        list2.add(new Producto(7L,"PS5",1200));
        list2.add(new Producto(8L,"Switch",800));
        list2.add(new Producto(9L,"Xbox S",1000));
        list2.add(new Producto(10L,"Xbox X",1200));
    }

    public Flux<Producto> buscarTecnologia(){
        return Flux.fromIterable(list).delayElements(Duration.ofSeconds(2));
    }

    public Flux<Producto> buscarVideojuegos(){
        return Flux.fromIterable(list2).delayElements(Duration.ofSeconds(3));
    }

}
