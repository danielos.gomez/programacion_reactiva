package com.programacion.reactiva.demo.control_excepciones;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public class Ejemplo8 {

    public static void main(String[] args) {

        // Devuelve un valor estático cuando ocurre un error.
        System.out.println("ErrorReturn");
        Flux.just(1, 2, 3)
                .map(i -> {
                    if (i == 2) throw new RuntimeException("Error!");
                    return i;
                })
                .onErrorReturn(-1) // Reemplaza con -1
                .subscribe(System.out::println);

        //Proporciona un flujo alternativo (fallback) cuando ocurre un error.
        System.out.println("\n \n ErrorResume");
        Flux.just(1,2,3,4,5)
                .concatWith(Flux.error(new Throwable("Ha ocurrido un error")))
                .concatWith(Mono.just(6))
                .onErrorResume(throwable -> {
                    return Mono.just(13);
                })
                .log()
                .subscribe();

        // Permite continuar procesando el flujo ignorando el error.
        System.out.println("\n \n ErrorContinue");
        Flux.just(1,2,3,4,5,6,7,8)
                .map(integer -> {
                    if (integer== 5){
                    throw new RuntimeException("Ha ocurrido un error");
                    }
                    return integer;
                })
                .onErrorContinue( (throwable, o) -> {
                    System.out.println("Detail Error" +throwable);
                    System.out.println("El elemento que causa la exepction es : "+o);
                })
                .log()
                .subscribe();

    }

}
