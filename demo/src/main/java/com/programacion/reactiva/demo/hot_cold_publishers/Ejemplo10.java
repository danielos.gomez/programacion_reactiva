package com.programacion.reactiva.demo.hot_cold_publishers;

import reactor.core.publisher.Flux;

import java.time.Duration;
import java.util.stream.Stream;

public class Ejemplo10 {

    public static void main(String[] args) throws InterruptedException {

        // hot pusblisher
        Flux<String> netFlux = Flux.fromStream(Ejemplo10::getVideo).delayElements(Duration.ofSeconds(2)).share();

        netFlux.subscribe(parte -> System.out.println("subscritor 1 : "+parte));
        Thread.sleep(7000);

        netFlux.subscribe(parte -> System.out.println("subscritor 2 : "+parte));
        Thread.sleep(12000);

    }

    private static Stream<String> getVideo(){
        System.out.println("Request para el video");
        return Stream.of("parte 1" , "parte 2", "parte 3", "parte 4" , "parte 5");
    }
    }


