package com.programacion.reactiva.demo.ejemplos_basicos;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.ArrayList;
import java.util.List;

public class Ejemplo1 {
    public static void main(String[] args) {
        List<Integer> elementosMono = new ArrayList<>();
        List<Integer> elementosFlux= new ArrayList<>();
        //Mono
        Mono<Integer> mono = Mono.just(12345) ;

        //Flux
        Flux<Integer> flux = Flux.just(1,2,3,4,5);

        mono.subscribe(elementosMono::add);
        flux.subscribe(elementosFlux::add);

        System.out.println(elementosMono);
        System.out.println(elementosFlux);
    }
}
