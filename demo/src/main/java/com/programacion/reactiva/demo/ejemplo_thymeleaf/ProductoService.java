package com.programacion.reactiva.demo.ejemplo_thymeleaf;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
@Service
public class ProductoService {

    @Autowired
    ProductoRepository productoRepository;

    public Flux<Producto> buscarTodos(){
        Flux<Producto> list1 = productoRepository.buscarTecnologia();
        Flux<Producto> list2 = productoRepository.buscarVideojuegos();
        return Flux.merge(list1,list2);
    }

}
