package com.programacion.reactiva.demo.contrapresion;

import reactor.core.publisher.Flux;

import java.util.ArrayList;
import java.util.List;

public class Ejemplo5 {

    public static void main(String[] args) {

        //Ejemplo sin contrapresión
        ArrayList<String> ciudadesList = new ArrayList<>(List.of("Cucuta", "Medellin","Bogota","Barranquilla","Cali"));
        Flux<String> ciudades = Flux.fromIterable(ciudadesList);

        ciudades.log().subscribe();
    }
}
