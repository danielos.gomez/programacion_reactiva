package com.programacion.reactiva.demo.transformacion_flujos.flatmap;

import java.util.ArrayList;
import java.util.List;

public class flatmatMain {
    public static void main(String[] args) {

        Cliente p = new Cliente("pedro");
        Viaje v = new Viaje("Francia");
        Viaje v2 = new Viaje("Inglaterra");
        p.addViaje(v);
        p.addViaje(v2);
        Cliente p1 = new Cliente("gema");
        Viaje v3 = new Viaje("Italia");
        Viaje v4 = new Viaje("Belgica");
        p1.addViaje(v3);
        p1.addViaje(v4);
        List<Cliente> lista = new ArrayList<Cliente>();
        lista.add(p);
        lista.add(p1);
        System.out.println("***** For Anidado*****");
        for (Cliente cliente : lista) {
            for (Viaje viaje : cliente.getLista()) {
                System.out.println(viaje.getPais());
            }
        }

        System.out.println("*****Flatmap*****");
        lista.stream()
                .map(Cliente::getLista)
                .flatMap(viajes -> viajes.stream())
                .forEach(viaje -> System.out.println(viaje.getPais()));


    }
}
