package com.programacion.reactiva.demo.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

@Data
@AllArgsConstructor
public class Persona {

    private String nombre;
    private String apellido;


}
