package com.programacion.reactiva.demo.ejemplo_thymeleaf;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.thymeleaf.spring6.context.webflux.IReactiveDataDriverContextVariable;
import org.thymeleaf.spring6.context.webflux.ReactiveDataDriverContextVariable;
@RequestMapping("/productos")
@Controller
public class ProductoController {
    @Autowired
    private ProductoRepository productoRepository;

    @Autowired
    private ProductoService productoService;

    @GetMapping("/listar/tecnologia")
    public String listarTecnologia(Model model){
        //variable reactiva
        System.out.println("entro");
        IReactiveDataDriverContextVariable listaReactiva = new ReactiveDataDriverContextVariable(productoRepository.buscarTecnologia(),1);
        model.addAttribute("listaProductos", listaReactiva);
        return "lista";

    }

    @GetMapping("/listar/videojuegos")
    public String listarVideojuegos(Model model){
        //variable reactiva
        System.out.println("entro");
        IReactiveDataDriverContextVariable listaReactiva = new ReactiveDataDriverContextVariable(productoRepository.buscarVideojuegos(),1);
        model.addAttribute("listaProductos", listaReactiva);
        return "lista";

    }

    @GetMapping("/listar/todo")
    public String listarTodo(Model model){
        //variable reactiva
        System.out.println("entro");
        IReactiveDataDriverContextVariable listaReactiva = new ReactiveDataDriverContextVariable(productoService.buscarTodos(),1);
        model.addAttribute("listaProductos", listaReactiva);
        return "lista";

    }


}
