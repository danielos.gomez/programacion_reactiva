package com.programacion.reactiva.demo;

import com.programacion.reactiva.demo.servicios.SimpleService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.test.StepVerifier;

import java.time.Duration;

@SpringBootTest
public class SimpleServiceTest {
@Autowired
    SimpleService simpleService;

    @Test
    void testMono(){
        Mono<String> uno = simpleService.buscarUno();
        StepVerifier.create(uno).expectNext("hola").verifyComplete();
    }
    @Test
    void testTodos(){
        Flux<String> varios = simpleService.buscarTodos();
        StepVerifier.create(varios).expectNext("hola").expectNext("mundo").expectComplete().verify();
    }

    @Test
    void testTodosLenta(){
        Flux<String> todosLento = simpleService.buscarTodosLento();
        StepVerifier.create(todosLento)
                .expectNext("hola").thenAwait(Duration.ofSeconds(1))
                .expectNext("mundo").thenAwait(Duration.ofSeconds(1))
                .verifyComplete();
    }


}
