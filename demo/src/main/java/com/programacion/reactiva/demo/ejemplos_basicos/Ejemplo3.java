package com.programacion.reactiva.demo.ejemplos_basicos;

import reactor.core.publisher.Mono;

import java.util.Random;
import java.util.function.Supplier;

public class Ejemplo3 {
    public static void main(String[] args) {

        Supplier<Integer> supplier = ()-> new Random().nextInt(100);

        System.out.println(supplier.get());



        //Mono -> Supplier
        Mono<String> mono = Mono.fromSupplier(() -> {
            throw new RuntimeException("Exception ocurrida lanzada desde Supplier");
        }
       );


        mono.subscribe(
                s -> System.out.println(s.toUpperCase()), //onNext
                throwable -> System.out.println("Error : "+throwable),//onError
                () -> System.out.println("Completed")//onComplete
        );



    }
}
