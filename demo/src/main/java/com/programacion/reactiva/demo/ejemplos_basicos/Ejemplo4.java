package com.programacion.reactiva.demo.ejemplos_basicos;

import reactor.core.publisher.Flux;

import java.util.ArrayList;
import java.util.List;

public class Ejemplo4 {
    public static void main(String[] args) {

        String[] myArray = {"Pedro", "Juan" , "Daniel","Mabel"};

        Flux<String> myFlux = Flux.fromArray(myArray);
        myFlux.subscribe(System.out::println);


        System.out.println("*********");

        ArrayList<String> myArrayList = new ArrayList<>(List.of("Pedro", "Juan" , "Daniel","Mabel"));
        Flux<String> myFlux2 = Flux.fromIterable(myArrayList);
        myFlux2.doOnNext(System.out::println).subscribe();

    }
}
