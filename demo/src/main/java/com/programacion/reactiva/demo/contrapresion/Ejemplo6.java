package com.programacion.reactiva.demo.contrapresion;

import org.reactivestreams.Subscription;
import reactor.core.publisher.BaseSubscriber;
import reactor.core.publisher.Flux;

import java.time.Duration;
import java.util.ArrayList;
import java.util.List;

public class Ejemplo6 {

    public static void main(String[] args) {
/*
        //Con Hook
        Flux<Integer> myFlux = Flux.range(1,100).log();
        myFlux.subscribe(new BaseSubscriber<Integer>() {
            @Override
            protected void hookOnSubscribe(Subscription subscription) {
                request(10);
            }
        });

        System.out.println("****************");

        // Con LimitRate
        Flux<Integer> myFlux2 = Flux.range(1, 20)
                .log()
                .limitRate(4);// Solicita 5 elementos a la vez con umbral del 75%
        myFlux2.subscribe(System.out::println);
/*


 */

        System.out.println("****************");

        // Con LimitRate y LowTide
        Flux<Integer> myFlux3 = Flux.range(1, 20)
                .log()
                .limitRate(5,0);// Solicita 5 elementos a la vez y hasta que no se procesen los 5 no pide mas
        myFlux3.subscribe(System.out::println);


    }




}

