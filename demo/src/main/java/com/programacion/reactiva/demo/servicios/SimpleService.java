package com.programacion.reactiva.demo.servicios;

import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.time.Duration;

@Service
public class SimpleService {

    public Mono<String> buscarUno(){
        return Mono.just("hola");
    }

    public Flux<String> buscarTodos(){
        return Flux.just("hola","mundo");
    }

    public Flux<String> buscarTodosLento(){
        return Flux.just("hola","mundo").delaySequence(Duration.ofSeconds(10));
    }
}
